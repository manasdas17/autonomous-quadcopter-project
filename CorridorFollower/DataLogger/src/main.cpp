#include <iostream>
#include <fstream>
#include <ros/ros.h>
#include "ardrone_autonomy/Navdata.h"
#include "Corridor/corridorFlyCommand.h"

using namespace std;
using namespace ros;



//---------------------------------------------------------------------------------------
// Functions are called everytime a new navdata/image is published
void navCallback(const ardrone_autonomy::Navdata::ConstPtr& usm)
{
	// Save to file some navigational data Vx, Vy, time 
	ofstream myfile;
	myfile.open ("nav.txt",ios::out | ios::app);
	// Time stamp, rotation Z, altitude, frame id, velocity of X and Y.
	myfile << usm->header.stamp <<","<< usm->rotZ << "," << usm->altd << "," << usm->vx << "," << usm->vy <<endl;
	myfile.close();
}

void flyCmdCallback(const Corridor::corridorFlyCommand::ConstPtr& usm)
{
	ofstream myfile;
	myfile.open ("imageProcess.txt",ios::out | ios::app);
	// time stamp of video frame having: VP oservation X & Y, VP estimation of X & Y.
	myfile << usm->frameTimeStamp << "," << usm->observationX << "," << usm->observationY << endl;
	myfile.close();
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "DataLogger");
	ros::NodeHandle nh;

	// Subscriber to navigational data.
	Subscriber sub1 = nh.subscribe("/railway/fly_command", 10, flyCmdCallback);
	Subscriber sub2 = nh.subscribe("/ardrone/navdata", 10, navCallback);

	ros::spin();
	ROS_INFO("tutorialROSOpenCV::main.cpp::No error.");
	while(ros::ok())
	{
	}	
}
