// To complie $ g++ `pkg-config --cflags opencv` -o RailwayImageProcess.o -c RailwayImageProcess.c `pkg-config --libs opencv`
// add on 64bit Ubuntu to above command: -fPIC
// To compile .so $ g++ RailwayImageProcess.o -Wall -g -shared -o RailwayImageProcess.so
// than copy to $ sudo cp RailwayImageProcess.so /usr/local/lib/libRailwayImageProcess.so


#ifndef RailwayImageProcess_H
#define RailwayImageProcess_H

#include <cxcore.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <map>
#include <math.h>
#include "cv.h"
#include "highgui.h"

using namespace cv;
using namespace std; 

// Crosspoint for 2 lines (l1 & l2)
Point * crossPoint(Vec4i l1, Vec4i l2);

// Probabilistic Hough line Transform 
// Parameters:image to be processed, treshold, min line length, max line length, window name where the lines, 
// crossing points on the img are shown, if u want the window. If tresh or min or max == 0 => valeus are 200, 45, 2.
vector<Vec4i> PHTlines(IplImage * img, int tresh,int min, int max, const char* win_name, bool drawWindow );

// Erode and/or Dilate the image with the e, d.
IplImage * ErodeDilate(IplImage *img,int e, int d);

// Laplace filter, 3X3 kernel, so filter size is 3, type can be 1,2,3,4 both positive and negative forms. v is the multipier.
// The kernels are in the **getFilterInt** function.
IplImage * laplaceFilter(IplImage *img, int filterSize,  int FilterType, int v);

// FAST Sobel operator implementation using inderect access, kernels are 3X3 and defined in the .cpp file.
 void Sobel_ind_fast( IplImage* img, IplImage* dst);
// Sobel using direct access using a pointer
 void Sobel_dir_fast( IplImage* img, IplImage* dst);

// SLOV Sobel operator implementation using inderect access
 void Sobel_ind( IplImage* img, IplImage* dst);
// Sobel using direct access using a pointer
 void Sobel_dir( IplImage* img, IplImage* dst);

// Ptocessing the PHL lines to detect VP and Yaw, The returnd Point(VP.x, Yaw).
// 'result' img is the image where we can draw our observation: lines, crossingpoint, VP rectangle.
Point2f LineProcessing(vector<Vec4i> lines, IplImage * result);
 
#endif

